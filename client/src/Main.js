import React, { Component } from 'react';

import ItemList from './ItemList';
import ItemForm from './ItemForm';
import Header from './Header';
import HttpClient from './HttpClient';

import './Main.css';

class Main extends Component {
  constructor() {
    super();
    this.state = {
      items: [],
      stores: [],
      selectedStore: undefined
    };

    this.handleOptimize = this.handleOptimize.bind(this);
    this.handleItemAdded = this.handleItemAdded.bind(this);
    this.handleItemRemoved = this.handleItemRemoved.bind(this);
    this.handleLoggedInChanged = this.handleLoggedInChanged.bind(this);
    this.handleStoreChanged = this.handleStoreChanged.bind(this);
    this.fetchStores = this.fetchStores.bind(this);
  }

  componentDidMount() {
    document.title = 'optimize your buy!';
    this.fetchStores();
  }

  getDisplayName(store) {
    if (!store) {
      return ''
    }
    return (store.info.name || '') + ' ' + (store.info.area || '') + ' ' + (store.info.city || '') + ' ' + (store.info.country || '');
  }

  render() {
    const storeOptions = this.state.stores.map(store => <option key={store._id}>{this.getDisplayName(store)}</option>);
    const selectedStoreDisplayName = this.getDisplayName(this.state.selectedStore);
    return (
      <div>
        <Header loggedIn={this.props.loggedIn} onLoggedInChanged={this.handleLoggedInChanged} />
        <div className="Main">
          <div className="DarkInfo">Your store</div>
          <select className="WideDropdown" name="store" size="1" onChange={this.handleStoreChanged} value={selectedStoreDisplayName}>
            {storeOptions}
          </select>
          <div className="VerticalSpacer" />
          <ItemForm onSubmit={this.handleItemAdded} selectedStore={this.state.selectedStore} />
          <div className="InfoText">
            <img className="ShoppingList" src={require('./assets/shopping-list.png')} alt="" />
            Your list
          </div>
          <ItemList items={this.state.items} onRemoveItem={this.handleItemRemoved} />
          <button className="BlueButton" onClick={this.handleOptimize}>Optimize</button>
        </div>
      </div>
    );
  }

  async fetchStores() {
    const json = await HttpClient.get('/stores', 0);
    const stores = json.stores || [];
    const first = stores.length > 0 ? stores[0] : undefined;
    this.setState({
      stores: stores,
      selectedStore: first
    });
  }

  async handleOptimize() {
    const response = await HttpClient.post('/optimize', {
      store: this.state.selectedStore._id,
      items: this.state.items,
    });
    console.log(response.items);
    this.setState({ items: response.items });
  }

  handleStoreChanged(event) {
    const storeInfoId = event.target.value;
    const store = this.findStoreByInfoId(storeInfoId);
    this.setState({ selectedStore: store });
  }

  handleItemRemoved(item) {
    this.setState({ items: this.state.items.filter(entry => entry.itemName !== item.itemName) })
  }

  handleItemAdded(item) {
    if (this.state.items.find(elem => elem.itemName === item.itemName)) {
      alert("Item already exists");
    } else {
      return this.setState({ items: this.state.items.concat(item) });
    }
  }

  handleLoggedInChanged(loggedIn) {
    this.fetchStores();
    this.props.onLoggedInChanged(loggedIn);
  }

  findStoreByInfoId(storeInfoId) {
    return this.state.stores.find(store => this.getDisplayName(store) === storeInfoId);
  }
}

export default Main;
