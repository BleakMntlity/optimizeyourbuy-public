import React, { Component } from 'react';

import './Login.css'

class Login extends Component {
    constructor() {
        super();
        this.state = {
            unfoldLogin: false,
            unfoldRegister: false,
            username: 'test@test.de',
            password: 'test',
            repeatedPassword: 'otherpassword',
            email: 'yourmail@mail.com',
        };

        this.handleUsernameChanged = this.handleUsernameChanged.bind(this);
        this.handlePasswordChanged = this.handlePasswordChanged.bind(this);
        this.handleLoginClicked = this.handleLoginClicked.bind(this);
        this.handleRegisterClicked = this.handleRegisterClicked.bind(this);
        this.handleEmailChanged = this.handleEmailChanged.bind(this);
        this.handleRepeatedPasswordChanged = this.handleRepeatedPasswordChanged.bind(this);
        this.handleLoginResponse = this.handleLoginResponse.bind(this);
    }

    handleLoginResponse(response) {
        if (response.status === 200) {
            this.props.onLoggedInChanged(true);
        } else if (response.status === 401) {
            alert('invalid username or password');
        } else {
            alert(response.status);
        }
    }

    handleLoginClicked() {
        if (this.state.unfoldLogin) {
            const body = {
                method: 'POST',
                credentials: 'same-origin',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: this.state.username,
                    password: this.state.password,
                })
            };

            fetch('users/login', body).then(response => {
                this.handleLoginResponse(response);
            });
        } else {
            this.setState({
                unfoldLogin: true,
                unfoldRegister: false
            });
        }
    }

    handleRegisterClicked() {
        if (this.state.unfoldRegister) {
            if (this.state.password !== this.state.repeatedPassword) {
                alert('passwords must match');
            } else {
                const body = {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        username: this.state.email,
                        password: this.state.password,
                    })
                };

                fetch('users/register', body).then(response => {
                    this.handleLoginResponse(response);
                });
            }
        } else {
            this.setState({
                unfoldLogin: false,
                unfoldRegister: true
            });
        }
    }

    handleUsernameChanged(event) {
        this.setState({ username: event.target.value });
    }

    handlePasswordChanged(event) {
        this.setState({ password: event.target.value });
    }

    handleRepeatedPasswordChanged(event) {
        this.setState({ repeatedPassword: event.target.value });
    }

    handleEmailChanged(event) {
        this.setState({ email: event.target.value });
    }

    getLoginElements() {
        const loginButton = <button className="LoginButton" onClick={this.handleLoginClicked}>Login</button>;
        if (!this.state.unfoldLogin) {
            return (
                <span>
                    {loginButton}
                </span>);
        } else {
            return (
                <span>
                    <input className="LoginInput" name="email" type="text" value={this.state.username} onChange={this.handleUsernameChanged} />
                    <input className="LoginInput" name="password" type="password" value={this.state.password} onChange={this.handlePasswordChanged} />
                    {loginButton}
                </span>
            );
        }
    }

    getRegisterElements() {
        const registerButton = <button className="LoginButton" onClick={this.handleRegisterClicked}>Register</button>
        if (!this.state.unfoldRegister) {
            return (
                <span>
                    {registerButton}
                </span>);
        } else {
            return (
                <span>
                    <input className="LoginInput" name="email" type="email" value={this.state.email} onChange={this.handleEmailChanged} />
                    <input className="LoginInput" name="password" type="password" value={this.state.password} onChange={this.handlePasswordChanged} />
                    <input className="LoginInput" name="repeatedPassword" type="password" value={this.state.repeatedPassword} onChange={this.handleRepeatedPasswordChanged} />
                    {registerButton}
                </span>
            )
        }
    }

    render() {
        if (this.props.loggedIn) {
            return <span className="SmallText"> Logged In </span>;
        }
        const login = this.getLoginElements();
        const register = this.getRegisterElements();
        return (
            <span>
                {login}
                {register}
            </span>
        );
    }
}

export default Login;