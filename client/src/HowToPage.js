import React, { Component } from 'react';
import Header from './Header';

import './Main.css';
import './HowToPage.css';

class HowToPage extends Component {
    render() {
        return (
            <div>
                <Header loggedIn={this.props.loggedIn} onLoggedInChanged={this.props.onLoggedInChanged}/>
                <div className="Main">
                    <p className="HowTo">Create a new store by adding the categories
                    as you pass them while walking through the store. Then you can enter
                    your shopping list at any time to retrieve a version that is ordered in
                    the way you find them in the store.
                  </p>
                </div>
            </div>
        )
    }
}

export default HowToPage;