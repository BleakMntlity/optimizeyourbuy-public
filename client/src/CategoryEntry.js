import React, { Component } from 'react';

import './Main.css';

class CategoryEntry extends Component {
    render() {
        return (
            <li className="ListEntry">
                {this.props.name} <button className="DeleteButton" onClick={() => this.props.onRemove(this.props.name)}/>
            </li>
        )
    }
}

export default CategoryEntry;