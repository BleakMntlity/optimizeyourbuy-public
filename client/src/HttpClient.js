async function post(url, body, timeout) {
    const request = {
        method: 'POST',
        timeout: timeout || 1000,
        body: JSON.stringify(body)
    };
    return send(url, request);
}

async function get(url, timeout) {
    const request = {
        method: 'GET',
        timeout: timeout || 1000
    };
    return send(url, request);
}

async function send(url, request) {
    request.credentials = 'same-origin';
    request.headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    };

    try {
        const response = await fetch(url, request);
        const json = await response.json();
        return json;
    } catch (err) {
        console.error(err);
        return {};
    }
}

export default {
    get: get,
    post: post
}