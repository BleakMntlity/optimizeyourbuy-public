import React, { Component } from 'react';

import './Main.css';

class ListEntry extends Component {
    render() {
        const item = this.props.item;
        const displayName = `${item.itemName} (${item.aisle})`;
        return (
            <li className="ListEntry">
                {displayName} <button className="DeleteButton" onClick={() => { this.props.onRemoveItem(item) }}/>
            </li>
        )
    }
}

export default ListEntry;