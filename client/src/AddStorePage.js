import React, { Component } from 'react';

import Header from './Header';
import CategoryEntry from './CategoryEntry'

import './Main.css'

class AddStorePage extends Component {
    constructor() {
        super();
        this.state = {
            storeName: 'name',
            storeArea: 'area',
            storeCity: 'city',
            storeCountry: 'country',
            currentAisle: 'aisle',
            aisles: []
        };

        this.handleSubmitStore = this.handleSubmitStore.bind(this);
        this.handleAisleAdded = this.handleAisleAdded.bind(this);
        this.handleAisleRemoved = this.handleAisleRemoved.bind(this);
        this.handleStoreNameChanged = this.handleStoreNameChanged.bind(this);
        this.handleStoreAreaChanged = this.handleStoreAreaChanged.bind(this);
        this.handleStoreCityChanged = this.handleStoreCityChanged.bind(this);
        this.handleStoreCountryChanged = this.handleStoreCountryChanged.bind(this);
        this.handleKeyDownInAisleField = this.handleKeyDownInAisleField.bind(this);
    }

    handleKeyDownInAisleField(event) {
        if (event.key === 'Enter') {
            this.handleAisleAdded(event);
        }
    }

    handleSubmitStore() {
        const submitBody = {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                store: {
                    info: {
                        name: this.state.storeName,
                        area: this.state.storeArea,
                        city: this.state.storeCity,
                        country: this.state.storeCountry
                    },
                    layout: this.state.aisles
                },
            })
        };

        const raiseError = () => alert('error while submitting store');
        fetch('/stores/add', submitBody)
            .then(res => {
                if (res.status !== 200) {
                    raiseError();
                } else {
                    alert('store added');
                }
            })
            .catch(error => raiseError);
    }

    handleAisleAdded(event) {
        const aisle = this.state.currentAisle;
        if (!this.state.aisles.find(entry => entry === aisle)) {
            this.setState({ aisles: this.state.aisles.concat(aisle) })
        } else {
            alert('category already added');
        }
        this.setState({currentAisle: ' '});
    }

    handleAisleRemoved(aisle) {
        const withoutAisle = this.state.aisles.filter(entry => entry !== aisle);
        this.setState({ aisles: withoutAisle });
    }

    handleStoreNameChanged(event) {
        this.setState({ storeName: event.target.value });
    }

    handleStoreAreaChanged(event) {
        this.setState({ storeArea: event.target.value });
    }

    handleStoreCityChanged(event) {
        this.setState({storeCity: event.target.value});
    }

    handleStoreCountryChanged(event) {
        this.setState({storeCountry: event.target.value});
    }

    render() {
        const aisles = this.state.aisles.map((entry) => {
            return <CategoryEntry name={entry} onRemove={this.handleAisleRemoved} />
        });
        return (
            <div>
                <Header loggedIn={this.props.loggedIn} onLoggedInChanged={this.props.onLoggedInChanged} />
                <div className="Main">
                    <div className="DarkInfo">Your store</div>
                    <span>
                        <input type='Text' value={this.state.storeName} onChange={this.handleStoreNameChanged} />
                        <input type='Text' value={this.state.storeArea} onChange={this.handleStoreAreaChanged} />
                    </span>
                    <br/>
                    <span>
                        <input type='Text' value={this.state.storeCity} onChange={this.handleStoreCityChanged} />
                        <input type='Text' value={this.state.storeCountry} onChange={this.handleStoreCountryChanged} />
                    </span>
                    <br />
                    <div className="VerticalSpacer" />
                    <input type='Text' value={this.state.currentAisle} onChange={event => this.setState({ currentAisle: event.target.value })} onKeyPress={this.handleKeyDownInAisleField}/>
                    <button className="AddButton" onClick={this.handleAisleAdded} />
                    <p className="InfoText">Layout:</p>
                    <ol>
                        {aisles}
                    </ol>
                    <div>
                        <button className="BlueButton" onClick={this.handleSubmitStore}>submit</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddStorePage;