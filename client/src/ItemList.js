import React, { Component } from 'react';
import ItemEntry from './ItemEntry';

class ItemList extends Component {
    render() {
        const items = this.props.items.map((item) => <ItemEntry
            item={item}
            onRemoveItem={this.props.onRemoveItem} />);
        return <ol>{items}</ol>;
    }
}

export default ItemList;