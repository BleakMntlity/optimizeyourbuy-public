import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Login from './Login';

import './Header.css';

class Header extends Component {
    render() {
        return (
            <div className="Header">
                <Login loggedIn={this.props.loggedIn} onLoggedInChanged={this.props.onLoggedInChanged} />
                <h1>
                    Optimize your buy
                    <img className="ShoppingCart" src={require('./assets/shopping-cart.png')} alt=""/>
                </h1>
                <Link to='/' className="Link">HOME</Link>
                <Link to='/add-store' className="Link"> Add store</Link>
                <Link to='/how-to' className="Link"> How to</Link>
            </div>
        )
    }
}

export default Header;