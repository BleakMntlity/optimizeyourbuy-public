import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import MainPage from './Main';
import AddStorePage from './AddStorePage';
import HowToPage from './HowToPage';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';

import HttpClient from './HttpClient';

class Index extends Component {
    constructor() {
        super();
        this.state = {
            loggedIn: false
        }
        this.handleLoggedInChanged = this.handleLoggedInChanged.bind(this);

    }

    componentDidMount() {
        this.fetchIsLoggedIn();
    }

    async fetchIsLoggedIn() {
        const result = await HttpClient.get('users/logged-in');
        const loggedIn = result.loggedIn;
        this.setState({ loggedIn: loggedIn });
    }

    handleLoggedInChanged(loggedIn) {
        this.setState({ loggedIn: loggedIn });
    }

    render() {
        const Main = (props) => {
            return <MainPage loggedIn={this.state.loggedIn} onLoggedInChanged={this.handleLoggedInChanged} />
        }

        const AddStore = (props) => {
            return <AddStorePage loggedIn={this.state.loggedIn} onLoggedInChanged={this.handleLoggedInChanged} />
        }

        const HowTo = (props) => {
            return <HowToPage loggedIn={this.state.loggedIn} onLoggedInChanged={this.handleLoggedInChanged} />
        }

        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' render={Main} />
                    <Route exact path='/add-store' render={AddStore} />
                    <Route exact path='/how-to' render={HowTo} />
                    <Route path='*' render={Main} />
                </Switch>
            </BrowserRouter>
        );
    }
}

ReactDOM.render((
    <Index />), document.getElementById('root'));
registerServiceWorker();
