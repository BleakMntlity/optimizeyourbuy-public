import React, { Component } from 'react';
import HttpClient from './HttpClient';

class ItemForm extends Component {
  constructor() {
    super();
    this.state = {
      itemName: 'item',
      aisle: undefined
    }
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleAisleChange = this.handleAisleChange.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleNameAdded = this.handleNameAdded.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  handleNameChange(event) {
    this.setState({
      itemName: event.target.value,
      aisle: this.state.aisle
    });
  }

  handleNameAdded(event) {
    this.fetchAisleIfNecessary();
  }

  handleKeyPress(event) {
    if (event.key === 'Enter') {
      if (this.shouldDetect()) {
        this.fetchAisleIfNecessary();
      } else {
        this.addItem();
      }
    }
  }

  async fetchAisleIfNecessary() {
    if (this.aisleIsUnknown()) {
      const storeId = this.props.selectedStore._id;
      const itemName = this.state.itemName;
      const response = await HttpClient.get(`items/aisle?storeId=${storeId}+itemName=${itemName}`);
      const isFocused = document.activeElement.id === 'InputShiftedLeft';
      if (!isFocused && this.aisleIsUnknown()) {
        this.setState({ aisle: response.aisle });
      }
    }
  }

  aisleIsUnknown() {
    return !this.state.aisle || this.state.aisle === 'unknown' || this.state.aisle === 'detect';
  }

  shouldDetect() {
    return !this.state.aisle || this.state.aisle === 'detect';
  }

  handleAisleChange(event) {
    this.setState({ aisle: event.target.value });
  }

  addItem() {
    this.props.onSubmit(this.state);
    this.setState({
      itemName: ' ',
      aisle: undefined
    });
  }

  async handleAdd(event) {
    await this.fetchAisleIfNecessary();
    this.addItem();
    event.preventDefault();
  }

  render() {
    const aisles = this.props.selectedStore ?
      this.props.selectedStore.layout.map(aisle => <option key={aisle} value={aisle}>{aisle}</option>) : [];

    let selectedAisle = this.state.aisle;
    if (!this.state.aisle) {
      aisles.push(<option key='detect'>detect</option>);
      selectedAisle = 'detect'
    } else if (this.state.aisle === 'unknown') {
      aisles.push(<option key='unkown'>unknown</option>)
    }

    return (
      <div>
        <div className="SameLine">
          <input type="text" value={this.state.itemName} onChange={this.handleNameChange}
            onBlur={this.handleNameAdded} onKeyPress={this.handleKeyPress} />
          <button className="AddButton" id="AddButtonShifted" onClick={this.handleAdd} />
        </div>
        <div className="SameLine">
          <select className="Dropdown" id="InputShiftedLeft" name="category" size="1" onChange={this.handleAisleChange} value={selectedAisle}>
            {aisles}
          </select>
        </div>
      </div>
    );
  }
}

export default ItemForm;