function mongoCollection(collection) {

    function findWithIdIn(ids) {
        return collection.find({ _id: { $in: ids } }).toArray();
    }

    function find(filter) {
        return collection.find(filter).toArray();
    }

    function findOne(filter) {
        return collection.findOne(filter);
    }

    function insert(element) {
        return collection.insert(element);
    }

    function save(element) {
        return collection.save(element);
    }

    return {
        findWithIdIn: findWithIdIn,
        find: find,
        findOne: findOne,
        insert: insert,
        save: save
    }
}

module.exports = { create: mongoCollection }