function user(user, db) {
    const users = db.collection('Users');

    async function addStore(store) {
        const knownStores = user.storeLayoutIDs || [];
        return users.save({
            _id: user._id,
            username: user.username,
            storeLayoutsIDs: knownStores.concat(store._id)
        });
    }

    return {
        username: user.username,
        storeLayoutIDs: user.storeLayoutIDs,
        addStore: addStore
    }
}

module.exports = { create: user };