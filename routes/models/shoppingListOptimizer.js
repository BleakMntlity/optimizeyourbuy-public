const ObjectID = require('mongodb').ObjectID;
const storesRepository = require('../repositories/storesRepository');

function shoppingListOptimizer(db) {
    const stores = storesRepository.create(db);

    function comparator(layout) {
        return (a, b) => {
            const idxOfA = layout.indexOf(a.aisle);
            const idxOfB = layout.indexOf(b.aisle);
            return idxOfA - idxOfB;
        }
    }

    async function optimize(shoppingList, storeId) {
        const store = await stores.getStore(new ObjectID(storeId));
        console.log(storeId);
        if (store) {
            return shoppingList.sort(comparator(store.layout));
        }
        console.log(shoppingList);
        return shoppingList;
    }

    return {
        optimize: optimize
    }
}

module.exports = { create: shoppingListOptimizer };