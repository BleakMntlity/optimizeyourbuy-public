const mongoCollection = require('./mongoCollection');

function mongoDB(db) {
    function collection(name) {
        return mongoCollection.create(db.collection(name));
    }

    return { 
        collection: collection 
    }
}

module.exports = { create: mongoDB };