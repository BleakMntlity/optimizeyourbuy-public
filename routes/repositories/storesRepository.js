const errors = require('../../common/errors');
const util = require('../../common/util');

function storesRepository(db) {
    const storeLayouts = db.collection('StoreLayouts');

    async function listStoreNames(user) {
        const storesById = await listAllStoresById();
        return Object.keys(storesById).map(key => {
            return getStoreInfoId(storesById[key][0]);
        });
    }

    async function listStores(user) {
        if (user) {
            const mostPopular = await findMostPopularLayouts();
            const userStoreIds = user.storeLayoutIDs;
            const userStores = await storeLayouts.findWithIdIn(userStoreIds || []);
            const result = mostPopular.filter(store => {
                const duplicate = userStores.find(userStore => {
                    return getStoreInfoId(userStore) === getStoreInfoId(store);
                });
                return duplicate === undefined;
            });
            return result.concat(userStores);
        } else {
            console.log('finding for no user');
            return findMostPopularLayouts();
        }
    }

    async function findMostPopularLayouts() {
        const storesByInfo = await listAllStoresById();
        return Object.keys(storesByInfo).map(key => {
            return storesByInfo[key].reduce((best, store) => {
                if (store.votes > best.votes) {
                    return store;
                }
                return best;
            })
        });
    }

    async function listAllStoresById() {
        const allLayouts = await storeLayouts.find();
        return allLayouts.reduce((acc, store) => {
            const sameInfo = acc[getStoreInfoId(store)] || [];
            sameInfo.push(store);
            acc[getStoreInfoId(store)] = sameInfo;
            return acc;
        }, {});
    }

    function getStoreInfoId(store) {
        const storeInfo = (store.info.name || '') + ' ' + (store.info.area || '') + ' ' + (store.info.city || '') + ' ' + (store.info.country || '');
        return storeInfo.trim();
    }

    async function insertStore(user, store) {
        const insertedStore = await storeLayouts.insert({
            info: store.info,
            layout: store.layout,
        });
        console.log(user);
        return user.addStore(insertedStore);
    }

    async function addStore(user, store) {
        if (!validate(store.info)) {
            throw errors.storeIncomplete;
        }
        return insertStore(user, store);
    }

    async function getLayout(user, store) {
        if (!util.isNullOrWhitespace(store._id)) {
            const inDBStore = await storeLayouts.findOne({ _id: store._id });
            if (!inDBStore) {
                throw errors.unknownStore;
            }
            return inDBStore.layout;
        } else {
            throw errors.storeIncomplete;
        }
    }

    function getStore(storeId) {
        return storeLayouts.findOne({ _id: storeId });
    }

    return {
        getStore: getStore,
        listStoreNames: listStoreNames,
        listStores: listStores,
        addStore: addStore,
        getLayout: getLayout
    }

    function validate(storeInfo) {
        return storeInfo && !(util.isNullOrWhitespace(storeInfo.name) || util.isNullOrWhitespace(storeInfo.area) ||
            util.isNullOrWhitespace(storeInfo.city) || util.isNullOrWhitespace(storeInfo.country));
    }
}



module.exports = { create: storesRepository };