const errors = require('../../common/errors');
const bcrypt = require('bcrypt');

function usersRepository(db) {
    const collection = db.collection('Users');

    async function register(user) {
        const username = user.username;
        const password = user.password;

        const existingUser = await collection.findOne({ username: username });
        if (existingUser) {
            throw errors.userExists;
        } else {
            const hashedPassword = await bcrypt.hash(password, 10);
            const insertedUser = await collection.insert({ username: username, password: hashedPassword });
            return insertedUser;            
        }
    }
    return {
        register: register
    }
}

module.exports = { create: usersRepository };