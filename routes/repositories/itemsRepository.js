function itemsRepository(db) {
    const items = db.collection('Items');

    function addItem(itemName, aisle, storeId) {
        const item = {
            name: itemName,
            aisles: [{
                aisle: aisle,
                votes: 1,
                voteSources: [{
                    storeId: storeId,
                    count: 1
                }]
            }]
        }
        return items.insert(item);
    }

    async function addVoteToItem(item, aisleName, storeId) {
        const aisle = item.aisles.find(aisle => aisle.aisle === aisleName);
        if (aisle) {
            addVotesSourceToAisle(aisle, storeId);
            return items.save(item);
        } else {
            const aisle = {
                aisle: aisleName,
                votes: 1,
                voteSources: [{
                    storeId: storeId,
                    count: 1
                }]
            };
            item.aisles.push(aisle);
            return items.save(item);
        }
    }

    function addVotesSourceToAisle(aisle, storeId) {
        ++aisle.votes;
        const storeVoteSource = aisle.voteSources.find(source => source.storeId === storeId);
        if (storeVoteSource) {
            ++storeVoteSource.count;
        } else {
            const voteSource = {
                storeId: storeId,
                count: 1
            }
            aisle.voteSources.push(voteSource);
        }
    }

    async function addVotes(votes, storeId) {
        const findPromises = votes.map(vote => items.findOne({ name: vote.itemName }));
        const dbItems = await Promise.all(findPromises);
        const updatePromises = votes.map(vote => {
            const dbItem = dbItems.find(dbItem => dbItem && dbItem.name === vote.itemName);
            if (dbItem) {
                return addVoteToItem(dbItem, vote.aisle, storeId);
            } else {
                return addItem(vote.itemName, vote.aisle, storeId);
            }
        });
        return Promise.all(updatePromises);
    }

    async function getAisle(itemName, storeId) {
        const item = await items.findOne({ name: itemName });
        if (item) {
            const aislesForItemAndStore = getAislesForItemAndStore(item, storeId);
            if (aislesForItemAndStore.length > 0) {
                const mostPopular = getMostPopularAisles(aislesForItemAndStore);
                if (mostPopular.length === 1) {
                    return mostPopular[0].aisle;
                } else {
                    const overallMorePopular = getOverallMorePopularAisle(mostPopular, item);
                    return overallMorePopular.aisle;
                }
            }
            return getMostPopularAisles(item.aisles)[0].aisle;
        }
        return 'unknown';
    }

    function getOverallMorePopularAisle(votesFromStore, item) {
        return votesFromStore.reduce((acc, aisle) => {
            const overallVotes = item.aisles.find(itemAilse => itemAilse.aisle === aisle.aisle);
            if (!acc || overallVotes.votes > aisle.votes) {
                return overallVotes;
            }
            return acc;
        });
    }

    function getMostPopularAisles(aisles) {
        return aisles.reduce((acc, aisle) => {
            if (!acc || aisle.votes > acc[0].votes) {
                return [aisle];
            } else if (aisle.votes == acc[0].votes) {
                return acc.concat(aisle);
            }
            return acc;
        }, null);
    }

    function getAislesForItemAndStore(item, storeId) {
        return item.aisles.reduce((acc, aisle) => {
            const voteSourcesFromStore = aisle.voteSources.filter(source => source.storeId === storeId);
            const countsFromVote = voteSourcesFromStore.map(source => {
                return {
                    aisle: aisle.aisle,
                    votes: source.count
                }
            });
            return acc.concat(countsFromVote);
        }, []);
    }

    return {
        addItem: addItem,
        addVoteToItem: addVoteToItem,
        addVotes: addVotes,
        getAisle: getAisle
    }
}

module.exports = { create: itemsRepository }