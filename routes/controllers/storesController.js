const express = require('express');
const storesRepository = require('../repositories/storesRepository');

function storesController(db) {
  const router = express.Router();
  const stores = storesRepository.create(db);

  router.get('/', async (req, res, next) => {
    try {
      const storeList = await stores.listStores(req.user);
      res.json({ stores: storeList });
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  });

  router.post('/edit', (req, res) => {
    res.sendStatus(500);
  })

  router.post('/add', async (req, res) => {
    if (!req.user) {
      res.sendStatus(401);
    }
    try {
      const store = await stores.addStore(req.user, req.body.store);
      if (store) {
        res.sendStatus(200);
      } else {
        res.sendStatus(400);
      }
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  });

  router.get('/layout', function (req, res) {
    stores.getLayout(req).then(layout => {
      if (res) {
        res.json(layout);
      } else {
        res.sendStatus(400);
      }
    })
  })
  return router;
}

module.exports = { create: storesController };
