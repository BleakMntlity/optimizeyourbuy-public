const express = require('express');
const errors = require('../../common/errors');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const userModel = require('../models/user');
const usersRepository = require('../repositories/usersRepository');

function usersController(db, passport) {
    const router = express.Router();
    const users = usersRepository.create(db);
    // todo move to repository
    const usersCollection = db.collection('Users');

    passport.use(new LocalStrategy(async (username, password, done) => {
        try {
            const sessionOwner = await usersCollection.findOne({ username: username });
            if (!sessionOwner) {
                done(null, false);
            } else {
                const passwordIsValid = await bcrypt.compare(password, sessionOwner.password);
                if (passwordIsValid) {
                    done(null, userModel.create(sessionOwner, db));
                } else {
                    done(null, false);
                }
            }
        } catch (err) {
            done(err, false)
        }
    }));

    passport.serializeUser((user, done) => {
        done(null, user.username);
    });
    passport.deserializeUser((username, done) => {
        usersCollection.findOne({ username: username }).then(user => {
            done(null, userModel.create(user, db));
        }).catch(err => {
            done(err, false);
        });
    });

    router.post('/register', async (req, res) => {
        try {
            const user = await users.register(req.body);
            if (user) {
                res.sendStatus(200);
            }
        } catch (err) {
            if (err.userExists) {
                res.status(400).json({ error: err });
            } else {
                res.sendStatus(500);
            }
        }
    });


    router.post('/login', passport.authenticate('local'), (req, res) => {
        res.sendStatus(200);
    });

    router.get('/logged-in', (req, res) => {
        if (req.isAuthenticated()) {
            res.json({ loggedIn: true });
        } else {
            res.json({ loggedIn: false });
        }
    })

    return router;
}

module.exports = { create: usersController };
