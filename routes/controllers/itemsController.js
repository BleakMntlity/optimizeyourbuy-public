const express = require('express');
const itemsRepository = require('../repositories/itemsRepository');

function itemsController(db) {
    const router = express.Router();
    const items = itemsRepository.create(db);

    router.get('/aisle', async (req, res) => {
        const storeId = req.query.storeId;
        const itemName = req.query.itemName;
        const aisle = await items.getAisle(itemName, storeId);
        res.json({ aisle: aisle });
    });
    
    return router;
}

module.exports = { create: itemsController };