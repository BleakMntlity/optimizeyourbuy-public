const express = require('express');
const shoppingListOptimizer = require('../models/shoppingListOptimizer');
const itemsRepository = require('../repositories/itemsRepository');

function optimizeController(db) {
    const router = express.Router();
    const optimizer = shoppingListOptimizer.create(db);
    const items = itemsRepository.create(db);

    router.post('/', async (req, res, next) => {
        const sortedList = await optimizer.optimize(req.body.items, req.body.store);
        res.json({items: sortedList});

        try {
            await items.addVotes(req.body.items, req.body.store);
            console.log('votes added');
        } catch (err) {
            console.log('couldnt add votes: ' + err);
        }
    });

    return router;
}

module.exports = { create: optimizeController };
