function applyFilters(keys, content) {
    const result = [];
    content.forEach(element => {
        let isValid = true;
        for (let key in keys) {
            isValid = isValid && element.hasOwnProperty(key) && element[key] === keys[key];
        }
        if (isValid) {
            result.push(element);
        }
    });
    return result;
}

function findResult(keys, content) {
    return {
        toArray: () => {
            return new Promise((resolve, reject) => {
                resolve(content);
            })
        }
    }
}

function dbCollection(content) {
    const saved = [];
    const inserted = [];
    return {
        savedObject: () => {
            if (saved.length > 0) {
                return saved[saved.length - 1];
            }
        },
        insertedObject: () => {
            if (inserted.length > 0) {
                return inserted[inserted.length - 1];
            }
        },
        savedObjects: () => saved,
        insertedObjects: () => inserted,
        findWithIdIn: (ids) => {
            return new Promise((resolve, reject) => {
                const res = content.filter(elem => ids.indexOf(elem._id) >= 0);
                resolve(res);
            });
        },
        find: () => {
            return new Promise((resolve, reject) => {
                resolve(content);
            })
        },
        findOne: (keys) => {
            return new Promise((resolve, reject) => {
                const result = applyFilters(keys, content);
                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    resolve(null);
                }
            });
        },
        save: (object) => {
            return new Promise((resolve, reject) => {
                saved.push(object);
                resolve(object);
            });
        },
        insert: (object) => {
            return new Promise((resolve, reject) => {
                inserted.push(object);
                resolve(object);
            });
        }
    }
}

function mockdb(content) {
    const self = {
        collections: {}
    };

    self.collection = (name) => {
        if (self.collections[name]) {
            return self.collections[name]
        }
        self.currentCollection = dbCollection(content[name] || content)
        self.collections[name] = self.currentCollection;
        return self.currentCollection;
    };

    return self;
}

module.exports = mockdb;