const assert = require('assert');
const createMockDb = require('./mocks/mockdb');
const errors = require('../common/errors');
const ItemsRepository = require('../routes/repositories/itemsRepository')

describe('connect item to aisles', () => {
    let testItems = undefined;
    let db = undefined
    let items = undefined;

    beforeEach(() => {
        testItems = createTestData();
        db = createMockDb(testItems);
        items = ItemsRepository.create(db);
    })

    it('add new item with an aisle', async () => {
        const insertedItem = await items.addItem('paprika', 'vegetables', '1');

        console.log(insertedItem);
        assert.equal(insertedItem.name, 'paprika'),
            assert.equal(insertedItem.aisles.length, 1);

        const firstAisle = insertedItem.aisles[0];
        assert.equal(firstAisle.aisle, 'vegetables');
        assert.equal(firstAisle.votes, 1);
        assert.equal(firstAisle.voteSources.length, 1);

        const firstVoteSource = firstAisle.voteSources[0];
        assert.equal(firstVoteSource.storeId, '1');
        assert.equal(firstVoteSource.count, 1);
    });

    it('add new aisle to an existing item', async () => {
        const testItem = testItems['Items'][1];
        const savedItem = await items.addVoteToItem(testItem, 'breakfast', '2');

        assert.equal(savedItem.name, testItem.name);
        assert.equal(savedItem.aisles.length, 2);

        const breakfast = savedItem.aisles.find(aisle => aisle.aisle === 'breakfast');
        assert.equal(breakfast.votes, 1);
        assert.equal(breakfast.voteSources.length, 1);
        assert.equal(breakfast.voteSources[0].storeId, '2');
        assert.equal(breakfast.voteSources[0].count, 1);
    });

    it('add new votes source to an existing aisle', async () => {
        const testItem = testItems['Items'][1];
        const savedItem = await items.addVoteToItem(testItem, 'coffee', '5');

        assert.equal(savedItem.name, testItem.name);
        assert.equal(savedItem.aisles.length, 1);

        assert.equal(savedItem.aisles[0].votes, 6);
        assert.equal(savedItem.aisles[0].voteSources.length, 5);

        let source = savedItem.aisles[0].voteSources.find(source => source.storeId === '5');
        assert.equal(source.count, 1);
        source = savedItem.aisles[0].voteSources.find(source => source.storeId === '1');
        assert.equal(source.count, 1);
        source = savedItem.aisles[0].voteSources.find(source => source.storeId === '2');
        assert.equal(source.count, 1);
        source = savedItem.aisles[0].voteSources.find(source => source.storeId === '3');
        assert.equal(source.count, 2);
        source = savedItem.aisles[0].voteSources.find(source => source.storeId === '4');
        assert.equal(source.count, 1);
    });

    it('add vote to existing vote source', async () => {
        const testItem = testItems['Items'][1];
        const savedItem = await items.addVoteToItem(testItem, 'coffee', '4');

        assert.equal(savedItem.name, testItem.name);
        assert.equal(savedItem.aisles.length, 1);

        assert.equal(savedItem.aisles[0].votes, 6);
        assert.equal(savedItem.aisles[0].voteSources.length, 4);

        source = savedItem.aisles[0].voteSources.find(source => source.storeId === '1');
        assert.equal(source.count, 1);
        source = savedItem.aisles[0].voteSources.find(source => source.storeId === '2');
        assert.equal(source.count, 1);
        source = savedItem.aisles[0].voteSources.find(source => source.storeId === '3');
        assert.equal(source.count, 2);
        source = savedItem.aisles[0].voteSources.find(source => source.storeId === '4');
        assert.equal(source.count, 2);
    });

    it('add votes to multiple items', async () => {
        const votes = [{
            itemName: 'baguette',
            aisle: 'bread',
        }, {
            itemName: 'espresso',
            aisle: 'breakfast',
        }, {
            itemName: 'paprika',
            aisle: 'vegetables',
        }];
        const updatedItems = await items.addVotes(votes, '2');
        console.log(testItems);
        //Baguette
        const baguette = updatedItems.find(item => item.name === 'baguette');
        assert.equal(baguette.aisles.length, 2);

        const bread = baguette.aisles.find(aisle => aisle.aisle === 'bread');
        assert.equal(bread.votes, 3);
        assert.equal(bread.voteSources.length, 3);

        const firstBreadSource = bread.voteSources.find(source => source.storeId === '1');
        assert.equal(firstBreadSource.count, 1);
        const secondBreadSource = bread.voteSources.find(source => source.storeId === '2');
        assert.equal(secondBreadSource.count, 1);
        const fourthBreadSource = bread.voteSources.find(source => source.storeId === '4');
        assert.equal(fourthBreadSource.count, 1);

        const food = baguette.aisles.find(aisle => aisle.aisle === 'food');
        assert.equal(food.votes, 3);
        assert.equal(food.voteSources.length, 1);

        const foodSource = food.voteSources.find(source => source.storeId === '3');
        assert.equal(foodSource.count, 3);
        //Espresso        
        const espresso = updatedItems.find(item => item.name === 'espresso');
        assert.equal(espresso.aisles.length, 2);

        const coffee = espresso.aisles.find(aisle => aisle.aisle === 'coffee');
        assert.equal(coffee.votes, 5);
        assert.equal(coffee.voteSources.length, 4);

        const firstCoffeeSource = coffee.voteSources.find(source => source.storeId === '1');
        assert.equal(firstCoffeeSource.count, 1);
        const secondCoffeeSource = coffee.voteSources.find(source => source.storeId === '2');
        assert.equal(secondCoffeeSource.count, 1);
        const thirdCoffeeSource = coffee.voteSources.find(source => source.storeId === '3');
        assert.equal(thirdCoffeeSource.count, 2);
        const fourthCoffeeSource = coffee.voteSources.find(source => source.storeId === '4');
        assert.equal(fourthCoffeeSource.count, 1);

        const breakfast = espresso.aisles.find(aisle => aisle.aisle === 'breakfast');
        assert.equal(breakfast.votes, 1);
        assert.equal(breakfast.voteSources.length, 1);

        const breakfastSource = breakfast.voteSources[0];
        assert.equal(breakfastSource.count, 1);
        assert.equal(breakfastSource.storeId, '2');
        //Paprika        
        const paprika = updatedItems.find(item => item.name === 'paprika');
        assert.equal(paprika.aisles.length, 1);

        const vegetables = paprika.aisles.find(aisle => aisle.aisle === 'vegetables');
        assert.equal(vegetables.votes, 1);
        assert.equal(vegetables.voteSources.length, 1);

        const vegetablesSource = vegetables.voteSources[0];
        assert.equal(vegetablesSource.count, 1);
        assert.equal(vegetablesSource.storeId, '2');
    })
});

describe('get aisle for an item', () => {
    let testItems = undefined;
    let db = undefined
    let items = undefined;

    beforeEach(() => {
        testItems = createTestData();
        db = createMockDb(testItems);
        items = ItemsRepository.create(db);
    });

    it('get unknown for unkown item', async () => {
        const aisle = await items.getAisle('paprika', '1');
        assert.equal('unknown', aisle);
    });

    it('get aisle for known item with one aisle', async () => {
        const ailse = await items.getAisle('espresso', '1');
        assert.equal(ailse, 'coffee');
    });

    it('get most popular aisle for item with no matching store', async () => {
        const aisle = await items.getAisle('baguette', '-1');
        assert.equal(aisle, 'food');
    });

    it('get most popular aisle for item for store', async () => {
        const aisle = await items.getAisle('baguette', '1');
        assert.equal(aisle, 'bread');
    });

    it('get overall more popular aisle if best popularity for store isnt unique', async () => {
        const baguette = testItems['Items'].find(item => item.name === 'baguette');
        await items.addVoteToItem(baguette, 'food', '1');

        const aisle = await items.getAisle('baguette', '1');
        assert.equal(aisle, 'food');
    })
})


function createTestData() {
    return {
        'Items': [{
            name: 'baguette',
            aisles: [{
                aisle: 'bread',
                votes: 2,
                voteSources: [{
                    storeId: '1',
                    count: 1
                }, {
                    storeId: '4',
                    count: 1
                }]
            }, {
                aisle: 'food',
                votes: 3,
                voteSources: [{
                    storeId: '3',
                    count: 3
                }]
            }]
        }, {
            name: 'espresso',
            aisles: [{
                aisle: 'coffee',
                votes: 5,
                voteSources: [{
                    storeId: '1',
                    count: 1
                }, {
                    storeId: '2',
                    count: 1
                }, {
                    storeId: '3',
                    count: 2
                }, {
                    storeId: '4',
                    count: 1
                }]
            }]
        }
        ]
    };
}