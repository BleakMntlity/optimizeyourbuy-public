const assert = require('assert');
const mockdb = require('./mocks/mockdb');
const users = require('../routes/repositories/usersRepository');
const bcrypt = require('bcrypt');

describe('test register new user', () => {
    it('cant register when email is already taken', async () => {
        const db = mockdb([{ username: 'chief@web.de', password: 'supersafe' }]);
        const user = { username: 'chief@web.de', password: 'test' };
        try {
            const insertedUser = await users.create(db).register(user);
            assert.fail();
        } catch (err) {
            assert.equal(true, true);
        }
    });

    it('can register a new user', async () => {
        const db = mockdb([{ username: 'chief@web.de', password: 'supersafe' }]);
        const user = { username: 'dude@web.de', password: 'test' };
        const insertedUser = await users.create(db).register(user);
        const insertedObj = db.currentCollection.insertedObject();
        assert.equal(user.username, insertedObj.username);
        const isSame = await bcrypt.compare(user.password, insertedObj.password);
        assert.equal(isSame, true);
    });
})