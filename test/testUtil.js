const assert = require('assert');
const util = require('../common/util');

describe('test null or whitespace', () => {
    it('true if string is null', () => {
        assert.equal(util.isNullOrWhitespace(null), true);
    });
    it('true if whitespace', () => {
        assert.equal(util.isNullOrWhitespace(' '), true);
    });
    it('true if empty', () => {
        assert.equal(util.isNullOrWhitespace(''), true);
    });
    it('false if any letter', () => {
        assert.equal(util.isNullOrWhitespace('s'), false);
    });
    it('false if any number', () => {
        assert.equal(util.isNullOrWhitespace('4'), false);
    });
    it('false if any special char', () => {
        assert.equal(util.isNullOrWhitespace('_'), false);
    });
})