const assert = require('assert');
const createMockDb = require('./mocks/mockdb');
const stores = require('../routes/repositories/storesRepository');
const user = require('../routes/models/user');
const errors = require('../common/errors');


describe('list stores for certain user', () => {
    const storeLayouts = [{ _id: '1', info: { name: 'Aldi' }, votes: 1 },
    { _id: '2', info: { name: 'Rewe' }, votes: 2 },
    { _id: '3', info: { name: 'Lidl' }, votes: 1 },
    { _id: '4', info: { name: 'Aldi' }, votes: 2 }];

    const db = createMockDb({ 'StoreLayouts': storeLayouts });
    const testUser = user.create({ username: 'test@test.de', storeLayoutIDs: ['1', '3'] }, db);
    const repository = stores.create(db);

    it('return all stores names', async () => {
        const names = await repository.listStoreNames(testUser);
        console.log(names);
        assert.equal(names.length, 3);
        assert.notEqual(names.indexOf('Rewe'), -1);
        assert.notEqual(names.indexOf('Aldi'), -1);
        assert.notEqual(names.indexOf('Lidl'), -1);
    });

    it('should return my stores and best', async () => {
        const stores = await repository.listStores(testUser);
        assert.equal(stores.length, 3);
        assert.notEqual(null, stores.find(store => store._id === '1'));
        assert.notEqual(null, stores.find(store => store._id === '2'));
        assert.notEqual(null, stores.find(store => store._id === '3'));
    });

    it('get proposed stores for unknown user', async () => {
        const stores = await repository.listStores(null);
        assert.equal(stores.length, 3);
        assert.notEqual(null, stores.find(store => store._id === '2'));
        assert.notEqual(null, stores.find(store => store._id === '3'));
        assert.notEqual(null, stores.find(store => store._id === '4'));
    });
});

describe('add stores', () => {
    const storeLayouts = [{ _id: '1', name: 'Aldi' }, { _id: '2', name: 'Rewe' }];
    const db = createMockDb(storeLayouts);
    const testUser = user.create({ username: 'test@test.de', storeLayoutIDs: ['1'] }, db);
    const testStore = {
        info: {
            name: 'Aldi',
            area: 'Muensterstrasze',
            city: 'Duesseldorf',
            country: 'Germany'
        },
        layout: ['coffee', 'vegetables'],
        items: []
    }
    const repository = stores.create(db);

    it('cant add empty store', (done) => {
        const store = { info: '' };
        const promise = repository.addStore(testUser, store)
            .catch(err => done());
    });
    it('can add new store with valid info', async () => {
        const res = await repository.addStore(testUser, testStore);
        const stores = db.collection('StoreLayouts');
        assert.equal(stores.insertedObject().info, testStore.info);
        assert.equal(stores.insertedObject().layout, testStore.layout);
        const users = db.collection('Users');
        assert.equal(users.savedObject().username, testUser.username);
    });
});


describe('get aisles for store', () => {
    const storeLayouts = [{ _id: '1', name: 'Aldi' }, { _id: '2', name: 'Rewe' }];
    const db = createMockDb(storeLayouts);
    const testUser = user.create({ username: 'test@test.de', storeLayoutIDs: ['1'] }, db);
    const testStore = { _id: '1' }
    const repository = stores.create(db);

    it('cant get aisles for unkown store', (done) => {
        testStore._id = '0'
        repository.getLayout(testUser, testStore).catch(err => {
            assert.equal(errors.unknownStore, err);
            done();
        });
    });

    it('cant get aisles if no id', (done) => {
        testStore._id = '';
        repository.getLayout(testUser, testStore).catch(err => {
            assert.equal(errors.storeIncomplete, err);
            done();
        });
    });

    it('returns aisles if valid id', async () => {
        testStore._id = '1';
        try {
            const layout = await repository.getLayout(testUser, testStore);
            assert.equal(testStore.layout, layout);
        } catch (err) {
            assert.fail();
        }
    })
});
