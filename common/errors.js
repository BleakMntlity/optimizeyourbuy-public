const errors = {
    itemHasNorName: 'item has no name',
    itemHasNoAisle: 'item has no catgory',
    storeIncomplete: 'store has incomplete data',
    internalError: 'internal error',
    userExists: 'user already exists',
    unknownUser: 'unkown user',
    unknownStore: 'unkown store'
};

module.exports = errors;
