function isNullOfWhitespace(string) {
    if (!string) {
        return true;
    }
    for (let i = 0; i < string.length; ++i) {
        const char = string[i];
        if (char !== ' ') {
            return false;
        }
    }
    return true;
}

module.exports = {
    isNullOrWhitespace: isNullOfWhitespace
};