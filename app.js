const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoClient = require('mongodb').MongoClient;
const nconf = require('nconf');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');

const optimizeController = require('./routes/controllers/optimizeController')
const storesController = require('./routes/controllers/storesController');
const usersController = require('./routes/controllers/usersController');
const itemsController = require('./routes/controllers/itemsController');
const mongoDB = require('./routes/models/mongoDB');

nconf.argv().env().file('keys.json');
const user = nconf.get('mongoUser');
const pass = nconf.get('mongoPass');
const host = nconf.get('mongoHost');
const port = nconf.get('mongoPort');
const sessionSecret = nconf.get('sessionSecret');

function getMongoUri() {
  if (process.env.NODE_ENV === 'production' && nconf.get('mongoDatabase')) {
    return `mongodb://${user}:${pass}@${host}:${port}/${nconf.get('mongoDatabase')}`;
  }
  return 'mongodb://localhost:27017/stupid-web';
}

const mongoUri = getMongoUri();
console.log('mongo URI is ' + mongoUri);
console.log('session secret is ' + sessionSecret);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'client/build')));

mongoClient.connect(mongoUri, (error, rawDatabase) => {
  const database = mongoDB.create(rawDatabase);
  if (!error) {
    app.use(session({
      store: new MongoStore({ url: mongoUri }),
      secret: sessionSecret,
      resave: false,
      saveUninitialized: false
    }));

    app.use(passport.initialize());
    app.use(passport.session());

    app.use('/stores', storesController.create(database));
    app.use('/optimize', optimizeController.create(database));
    app.use('/users', usersController.create(database, passport));
    app.use('/items', itemsController.create(database));

    app.get('*', (req, res) => {
      res.sendFile(path.join(__dirname + '/client/build.index.html'))
    });

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
      var err = new Error('Not Found');
      err.status = 404;
      next(err);
    });

    // error handler
    app.use(function (err, req, res, next) {
      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};

      // log the message to the console
      console.log(err.message);

      // render the error page
      res.status(err.status || 500);
      res.render('error');
    });
  } else {
    console.log(error);
  }
})
module.exports = app;
